package prj3part2;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

/*******************************************************************
 * The Server class is a server for a TCP encrypted chat server.
 * It receives encrypted messages from a client and then forwards
 * the message to the designated client or broadcast to all clients
 * connected to the server.
 * @author Kyle Hekhuis
 * @since 10/29/16
 ********************************************************************/
public final class Server {

	/** A thread safe hash map containing all clients connected to this server. */
	public static volatile ConcurrentHashMap<Long, ClientInfo> connectedClients;
	/** Private key used for decrypting messages encrypted with the public key. */
	private static PrivateKey privKey;
	/** Public key used for encrypting messages. */
    private PublicKey pubKey;
	
	/*******************************
	 * Creates a new Server object.
	 *******************************/
	@SuppressWarnings("resource")
	private Server() {
		//Reader for user input
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		Boolean validPort = false;
		String portNumber = "";
		ServerSocket listenSocket = null;
		System.out.println("SERVER: Please enter a port number to listen on: ");
		//Getting a port number for the server from the user and checks if port is valid
		while (!validPort) {
			try {
				portNumber = inFromUser.readLine();
				listenSocket = new ServerSocket(Integer.parseInt(portNumber));
				System.out.println("SERVER: Listening for connections on port " + portNumber + ".");
				validPort = true;
			} catch (NumberFormatException | IOException e1) {
				System.out.println("SERVER: Not a valid port to use / Port unavailable.\n"
						+ "Please enter a different port number: ");
			}
		}
		//Set encryption keys for server
		setPrivateKey("src/RSApriv.der");
		setPublicKey("src/RSApub.der");
		//Gets a client connection and starts a new thread for each client
	    Socket clientSocket = null;
	    ClientListener cl;
	    connectedClients = new ConcurrentHashMap<Long, ClientInfo>();
	    ClientListChangeListener listChangeListener = new ClientListChangeListener();
	    listChangeListener.start();
	    while (true) {  
	    	try {
				clientSocket = listenSocket.accept();
				System.out.println("SERVER: Client connected.");
				cl = new ClientListener(clientSocket, clientSocket.getRemoteSocketAddress().toString());
				cl.start();
				//Adds connected client to the connected clients list
				connectedClients.put(cl.getId(), new ClientInfo(clientSocket.getRemoteSocketAddress().toString(), cl));
				/*
				int count = 1;
				for(Entry<Long, ClientInfo> entry : connectedClients.entrySet()) {
					System.out.println("SERVER: Client: " + count);
					count++;
					System.out.println("SERVER: Client Key: " + entry.getKey());
				    System.out.println("SERVER: Client IP: " + entry.getValue().clientIP);
				
				}
				*/
			} catch (IOException e) {
				System.out.println("SERVER: Failed to accept client on socket.");
			}
    	}
	}
	
	/********************************************
	 * Sets the private key using RSApriv.der
	 * @param filename file path for RSApriv.der
	 * @author Andrew Kalafut
	 ********************************************/
	private void setPrivateKey(final String filename){
		try {
		    File f = new File(filename);
		    FileInputStream fs = new FileInputStream(f);
		    byte[] keybytes = new byte[(int) f.length()];
		    fs.read(keybytes);
		    fs.close();
		    PKCS8EncodedKeySpec keyspec = new PKCS8EncodedKeySpec(keybytes);
		    KeyFactory rsafactory = KeyFactory.getInstance("RSA");
		    privKey = rsafactory.generatePrivate(keyspec);
		} catch (Exception e) {
		    System.out.println("Private Key Exception");
		    e.printStackTrace(System.out);
		    System.exit(1);
		}
    }
    
	/*******************************************
	 * Sets the public key using RSApub.der
	 * @param filename file path for RSApub.der
	 * @author Andrew Kalafut
	 *******************************************/
    private void setPublicKey(final String filename){
		try {
		    File f = new File(filename);
		    FileInputStream fs = new FileInputStream(f);
		    byte[] keybytes = new byte[(int) f.length()];
		    fs.read(keybytes);
		    fs.close();
		    X509EncodedKeySpec keyspec = new X509EncodedKeySpec(keybytes);
		    KeyFactory rsafactory = KeyFactory.getInstance("RSA");
		    pubKey = rsafactory.generatePublic(keyspec);
		} catch (Exception e) {
		    System.out.println("Public Key Exception");
		    System.exit(1);
		}
    }
    
    /********************************************************
     * Decrypts a byte array using the server's private key.
     * @param ciphertext byte array to decrypt
     * @return decrypted byte array
     * @author Andrew Kalafut
     ********************************************************/
    public static byte[] RSADecrypt(final byte[] ciphertext){
		try {
		    Cipher c = Cipher.getInstance("RSA/ECB/OAEPWithSHA-1AndMGF1Padding");
		    c.init(Cipher.DECRYPT_MODE, privKey);
		    byte[] plaintext = c.doFinal(ciphertext);
		    return plaintext;
		} catch (Exception e) {
		    System.out.println("RSA Decrypt Exception");
		    System.exit(1);
		    return null;
		}
    }
    
    /***************************************************************************
     * Takes an unencrypted byte array and returns an encrypted version of it.
     * @param plaintext byte array to encrypt
     * @param secKey Secret key to use as part of cipher for encrypting
     * @param iv initialization vector to use as part of cipher for encrypting
     * @return encrypted byte array
     * @author Andrew Kalafut
     ***************************************************************************/
    public static byte[] encrypt(final byte[] plaintext, 
    							 final SecretKey secKey, 
    							 final IvParameterSpec iv) {
		try {
		    Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		    c.init(Cipher.ENCRYPT_MODE, secKey, iv);
		    byte[] ciphertext = c.doFinal(plaintext);
		    return ciphertext;
		} catch (Exception e) {
		    System.out.println("AES Encrypt Exception");
		    System.exit(1);
		    return null;
		}
    }
    
    /***************************************************************************
     * Takes an encrypted byte array and returns an unencrypted version of it.
     * @param ciphertext byte array to be decrypted
     * @param secKey Secret key to use as part of cipher for decrpyting
     * @param iv initialization vector to use as part of cipher for decrypting
     * @return decrypted byte array
     * @author Andrew Kalafut
     ***************************************************************************/
    public static byte[] decrypt(final byte[] ciphertext, 
    							 final SecretKey secKey, 
    							 final IvParameterSpec iv) {
		try {
		    Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
		    c.init(Cipher.DECRYPT_MODE, secKey, iv);
		    byte[] plaintext = c.doFinal(ciphertext);
		    return plaintext;
		} catch (Exception e) {
		    System.out.println("AES Decrypt Exception");
		    System.exit(1);
		    return null;
		}
    }
	
	/*********************************************
	 * Runs the server.
	 * @param args arguments for the main method
	 ********************************************/
	public static void main(final String[] args) {
		Server s = new Server();
	}
}
 /***********************************************************************
  * The ClientListener class is a thread that listens for messages from
  * a connected client and reads identifier tags on the messages then
  * decides what to do with them based on the tags.
  * @author Kyle Hekhuis
  * @since 10/29/16
  ************************************************************************/
final class ClientListener extends Thread {
	
	/** Socket to listen on. */
	private Socket clientSocket;
	/** The IP of the socket this thread listens to. */
	private String thisClientIP;
	/** Data output to server. */
	private DataOutputStream dos;
	/** Data input from server. */
	private DataInputStream dis;
	/** The secret key of the client this thread listens to. */
	private SecretKey secretKey;
	/** The IV of the client this thread listens to. */
	private IvParameterSpec serverIV;
	/** Random generator to use for IV. */
	private SecureRandom r;
	/** Byte array to use for generating IV. */
	private byte[] ivbytes;
	/** Boolean to check if this client has had its secret key set or not. */
	private boolean secretKeySet;
	
	/******************************************************************
	 * Creates a ClientListener object from the passed socket and IP.
	 * @param connection the socket this thread listens to
	 * @param clientIP the IP of the socket this thread listens to
	 ******************************************************************/
	ClientListener(final Socket connection, final String clientIP) {
		this.clientSocket = connection;
		this.thisClientIP = clientIP.substring(1);
		r = new SecureRandom();
		ivbytes = new byte[16];
		secretKeySet = false;
	}
	
	/*******************************************************************
	 * Gets messages from the clientSocket and handles the information.
	 *******************************************************************/
	public void run() {
		String clientMessage = "";
		try {
			//Instantiate data streams
		    dos = new DataOutputStream(clientSocket.getOutputStream());
		    dis = new DataInputStream(clientSocket.getInputStream());
			
		    //Get secret key from client
			byte[] encryptedKey = readBytes();
			byte[] decryptedKey = Server.RSADecrypt(encryptedKey);
			secretKey = new SecretKeySpec(decryptedKey, "AES");
			secretKeySet = true;
			
			IvParameterSpec serverIV;
			while (true) {
				//Get IV from client
				byte[] ivBytes = readBytes();
				serverIV = new IvParameterSpec(ivBytes);
				//Get message from client
				byte[] encryptedMessage = readBytes();
				byte[] decryptedMessage = Server.decrypt(encryptedMessage, secretKey, serverIV);
				clientMessage = new String(decryptedMessage);
				int index1 = clientMessage.indexOf("#");
				int index2 = clientMessage.indexOf("#", index1 + 1);
				//Get an identifier string to see what type of message it is
				String identifier = clientMessage.substring(index1 + 1, index2);
				//System.out.println("SERVER: Identifier: " + identifier);
				/*
				 * IP
				 * Get the IP of this client and send back to them.
				 */
				if (identifier.equals("GET_IP")) {
					//System.out.println("SERVER: IP HERE");
					forwardMessage("#IP#" + thisClientIP);
				}
				/*
				 * GET_CLIENTS
				 * Get list of clients connected to server and send list to client.
				 */
				else if (identifier.equals("GET_CLIENTS")) {
					//System.out.println("SERVER: GET_CLIENTS HERE");
					String clientList = "#CLIENT_LIST#";
					for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
					    clientList += entry.getValue().getClientIP() + ",";
					    forwardMessage(clientList);
					}
				}
				/*
				 * TERMINATE
				 * End this connection.
				 */
				else if (identifier.equals("TERMINATE")) {
					//System.out.println("SERVER: TERMINATE HERE");
					Server.connectedClients.remove(currentThread().getId());
					clientSocket.close();
				}
				/*
				 * ADMIN_KICK
				 * Disconnect the desired client from the server and remove
				 * them from server's connected clients list.
				 */
				else if (identifier.equals("ADMIN_KICK")) {
					//System.out.println("SERVER: ADMIN_KICK HERE");
					String clientToKick = clientMessage.substring(index2 + 1);
					Long clientToKickKey = null;
					for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
					    if (entry.getValue().getClientIP().equals(clientToKick)){
					    	clientToKickKey = entry.getKey();
					    }
					}
					try {
						//Server.connectedClients.get(clientToKickKey).clientThread.interrupt();
						Server.connectedClients.get(clientToKickKey).getClientThread().getClientSocket().close();
						Server.connectedClients.remove(clientToKickKey);
					} catch (Exception e) {
						System.out.println("SERVER: Failed to kick client.");
					}
					
				}
				/*
				 * BROADCAST
				 * Send this message out to every client connected to the server
				 * and add a tag of which client it's from.
				 */
				else if (identifier.equals("BROADCAST")) {
					//System.out.println("SERVER: BROADCAST HERE");
					String newMessage = "#BROADCAST#<" + thisClientIP + ">" + clientMessage.substring(index2 + 1);
					for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
						entry.getValue().getClientThread().forwardMessage(newMessage);
					}
				}
				/*
				 * MESSAGE
				 * Send this message to the designated client and add a tag
				 * of which client it's from.
				 */
				else if (identifier.equals("MESSAGE")) {
					//System.out.println("SERVER: MESSAGE HERE");
					int index3 = clientMessage.indexOf("<");
					int index4 = clientMessage.indexOf(">");
					String clientToSendTo = clientMessage.substring(index3 + 1, index4);
					String newMessage = "#MESSAGE#<" + thisClientIP + ">" + clientMessage.substring(index4 +1);
					for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
					    if (entry.getValue().getClientIP().equals(clientToSendTo)) {
					    	entry.getValue().getClientThread().forwardMessage(newMessage);
					    }
					}
				}
	    	}
		} catch (Exception e) {
			//Remove this client if any errors occur
			Server.connectedClients.remove(currentThread().getId());
			System.out.println("SERVER: Client disconnected.");
		}
	}
	
	/**********************************************
	 * Encrypts a passed message and then sends it 
	 * to the client this thread is connected to.
	 * @param message message to forward to client
	 **********************************************/
	public void forwardMessage(final String message) {
		try {
			r.nextBytes(ivbytes);
			serverIV = new IvParameterSpec(ivbytes);
			sendBytes(serverIV.getIV());
			System.out.println("SERVER: Unencrypted message: " + message);
			byte[] encryptedMessage = Server.encrypt(message.getBytes(), secretKey, serverIV);
			System.out.println("SERVER: Encrypted message: " + DatatypeConverter.printHexBinary(encryptedMessage));
			sendBytes(encryptedMessage);
			
		} catch (IOException e) {
			System.out.println("SERVER: Failed to send message");
		}
    }
	
	/***************************************
	 * Returns clientSocket for this thread.
	 * @return clientSocket of this thread
	 ***************************************/
	public Socket getClientSocket() {
		return clientSocket;
	}
	
	/*********************************************************************************************************************
	 * Sends the passed byte array over the socket.
	 * @param myByteArray byte array to send to client
	 * @throws IOException if DataOutputStream write() fails
	 * @author Jason Plank  & Kevin Brock on StackOverflow
	 * https://stackoverflow.com/questions/2878867/how-to-send-an-array-of-bytes-over-a-tcp-connection-java-programming
	 **********************************************************************************************************************/
	private void sendBytes(final byte[] myByteArray) throws IOException {
	    int len = myByteArray.length;
	    dos.writeInt(len);
	    if (len > 0) {
	        dos.write(myByteArray, 0, len);
	    }
	}
	
	/*******************************************************************************************************************
	 * Reads data sent to this socket.
	 * @return byte array of data read from socket
	 * @throws IOException if DataInputStream readFully() fails
	 * @author Jason Plank  & Kevin Brock on StackOverflow
	 * https://stackoverflow.com/questions/2878867/how-to-send-an-array-of-bytes-over-a-tcp-connection-java-programming
	 *******************************************************************************************************************/
	private byte[] readBytes() throws IOException {
	    int len = dis.readInt();
	    byte[] data = new byte[len];
	    if (len > 0) {
	        dis.readFully(data);
	    }
	    return data;
	}
	
	/**********************************
	 * Returns secret key set boolean.
	 * @return secret key set boolean
	 **********************************/
	public boolean getSecretKeySet() {
		return secretKeySet;
	}
}

/******************************************************************
 * The ClientListChangeListener class is a thread that checks for
 * changes to the connectedClients list on the server. Sends
 * notification to client if there is a change.
 * @author Kyle Hekhuis
 * @since 11/14/16
 *****************************************************************/
final class ClientListChangeListener extends Thread {
    public void run() {
	    int size = Server.connectedClients.size();
		while (true) {
			if (Server.connectedClients.size() != size) {
				String clientList = "#CLIENT_LIST#";
				for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
				    clientList += entry.getValue().getClientIP() + ",";
				}
				for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
					if (entry.getValue().getClientThread().getSecretKeySet()) {
						entry.getValue().getClientThread().forwardMessage(clientList);
					}
				}
				size = Server.connectedClients.size();
			}
		}
    }
}