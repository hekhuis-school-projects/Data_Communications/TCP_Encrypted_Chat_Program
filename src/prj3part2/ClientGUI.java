package prj3part2;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;

/*********************************************************
 * The ClientGUI class is a GUI representation of Client.
 * @author Kyle Hekhuis
 * @since 10/29/16
 *********************************************************/
public final class ClientGUI {

	/** Amount of rows for the message tabs. */
	private static final int MESSAGE_AREA_ROWS = 12;
	/** Amount of columns for the message tabs. */
	private static final int MESSAGE_AREA_COLUMNS = 50;
	/** Client backend used to send / receive data from server. */
	private Client client;
	/** Frame for the GUI. */
	private JFrame frame;
	/** Main panel that contains all other panels. */
	private JPanel mainPanel; 
	/** Panel for the list of available clients. */
	private JPanel availableClientsPanel; 
	/** Panel for the send and broadcast buttons. */
	private JPanel buttonPanel;
	/** Panel for received messages and messages to send. */
	private JPanel messagePanel;
	/** Menu bar for GUI. */
	private JMenuBar menuBar;
	/** File menu for menu bar. */
	private JMenu fileMenu;
	/** Admin menu for menu bar. */
	private JMenu adminMenu;
	/** Help menu for menu bar. */
	private JMenu helpMenu;
	/** Exit item for fileMenu. */
	private JMenuItem exitMenuItem;
	/** Update Client List item for fileMenu. */
	private JMenuItem updateClientsMenuItem;
	/** Remove selected tab item for fileMenu. */
	private JMenuItem removeTabMenuItem;
	/** Admin kick item for adminMenu. */
	private JMenuItem adminKickMenuItem;
	/** Admin help item for adminMenu. */
	private JMenuItem adminHelpMenuItem;
	/** About item for helpMenu. */
	private JMenuItem aboutMenuItem;
	/** Button to send message to selected client. */
	private JButton sendButton;
	/** Button to broadcast message to all clients. */
	private JButton broadcastButton;
	/** Text area for messages to send. */
	private JTextArea messageSendText;
	/** Text area for received broadcast messages. */
	private JTextArea broadcastMessageText;
	/** Label displaying this client's IP. */
	private JLabel clientLabel;
	/** Label for available clients. */
	private JLabel availableClientsLabel;
	/** Label for message sending area. */
	private JLabel messageSendLabel;
	/** Scroll pane for received broadcast messages. */
	private JScrollPane broadcastMessageScrollPane;
	/** Scroll pane for messages to send. */
	private JScrollPane messageSendScrollPane;
	/** Scroll pane for available clients list. */
	private JScrollPane availableClientsScrollPane;
	/** Tabbed pane for messages from different clients. */
	private JTabbedPane messageTabbedPane;
	/** List of available clients. */
	private JList<String> availableClientsList;
	/** Model to hold list of available clients. */
	private DefaultListModel<String> availableClientsModel;
	/** Listener for buttons in the GUI. */
	private ButtonListener buttonListener;
	/** Listener for Client object for server messages. */
	private MessageListener messageListener;
	
	/******************************
	 * Creates a ClientGUI object.
	 ******************************/
	private ClientGUI() {
		try {
			String ip = JOptionPane.showInputDialog("IP Address to connect to:");
			String port = JOptionPane.showInputDialog("Port to connect to:");
			//System.out.println(ip);
			//System.out.println(port);
			client = new Client(ip, port);
		} catch (Exception e) { //Shuts down program if failure to create client socket
			JOptionPane.showMessageDialog(frame,
		    "Failed to connect to server.\n"
		    + "Terminating client session.",
		    "Connection Failure",
		    JOptionPane.ERROR_MESSAGE);
			System.exit(0);
		}
		buttonListener = new ButtonListener();
		messageListener = new MessageListener();
		//Add listener for server messages
		client.addPropertyChangeListener(messageListener);
		frame = new JFrame("Project 3: TCP Encrypted Chat");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setupMenuBar();
		setupPanels();
		frame.add(mainPanel);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setResizable(true);
		frame.setVisible(true);
	}
	
	/*************************************************
	 * Sets up the menu bar and adds it to the frame.
	 *************************************************/
	private void setupMenuBar() {
		//File Menu
		fileMenu = new JMenu("File");
		updateClientsMenuItem = new JMenuItem("Update Client List");
		updateClientsMenuItem.addActionListener(buttonListener);
		//Uncomment following line if using timer instead of manual update for client list
		//updateClientsMenuItem.setEnabled(false); 
		removeTabMenuItem = new JMenuItem("Remove Tab");
		removeTabMenuItem.addActionListener(buttonListener);
		exitMenuItem = new JMenuItem("Exit");
		exitMenuItem.addActionListener(buttonListener);
		fileMenu.add(updateClientsMenuItem);
		fileMenu.add(removeTabMenuItem);
		fileMenu.addSeparator();
		fileMenu.add(exitMenuItem);
		
		//Admin Menu
		adminMenu = new JMenu("Admin");
		adminKickMenuItem = new JMenuItem("Kick Client");
		adminKickMenuItem.addActionListener(buttonListener);
		adminHelpMenuItem = new JMenuItem("Admin Help");
		adminHelpMenuItem.addActionListener(buttonListener);
		adminMenu.add(adminKickMenuItem);
		adminMenu.addSeparator();
		adminMenu.add(adminHelpMenuItem);
		
		//Help Menu
		helpMenu = new JMenu("Help");
		aboutMenuItem = new JMenuItem("About");
		aboutMenuItem.addActionListener(buttonListener);
		helpMenu.add(aboutMenuItem);
		
		//Menu Bar
		menuBar = new JMenuBar();
		menuBar.add(fileMenu);
		menuBar.add(adminMenu);
		menuBar.add(helpMenu);
		menuBar.add(Box.createHorizontalGlue());
		clientLabel = new JLabel("Current Client: ");
		client.getClientIP();
		menuBar.add(clientLabel);
		frame.setJMenuBar(menuBar);
	}
	
	/**********************************************************
	 * Sets up all the panels and adds them to the main panel.
	 **********************************************************/
	private void setupPanels() {
		setupMessagePanel();
		setupAvailableClientsPanel();
		setupButtonPanel();
		//Right hand panel contains available client list and buttons
		//In contrast to left hand panel which is just message panel
		JPanel righthandPanel = new JPanel();
		righthandPanel.setLayout(new BoxLayout(righthandPanel, BoxLayout.Y_AXIS));
		righthandPanel.add(Box.createRigidArea(new Dimension(0,5)));
		righthandPanel.add(availableClientsPanel);
		righthandPanel.add(Box.createRigidArea(new Dimension(0,10)));
		righthandPanel.add(buttonPanel);
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
		mainPanel.add(messagePanel, BorderLayout.LINE_START);
		mainPanel.add(Box.createRigidArea(new Dimension(10,0)), BorderLayout.CENTER);
		mainPanel.add(righthandPanel, BorderLayout.LINE_END);
	}
	
	/****************************
	 * Sets up the message panel.
	 ****************************/
	private void setupMessagePanel() {
		messagePanel = new JPanel();
		messagePanel.setLayout(new BoxLayout(messagePanel, BoxLayout.Y_AXIS));
		
		messageTabbedPane = new JTabbedPane();
		
		broadcastMessageText = new JTextArea(MESSAGE_AREA_ROWS, MESSAGE_AREA_COLUMNS);
		broadcastMessageText.setEditable(false);
		broadcastMessageScrollPane = new JScrollPane(broadcastMessageText);
		
		messageTabbedPane.add("Broadcast Messages", broadcastMessageScrollPane);
		
		messageSendLabel = new JLabel("Type your message here:");
		messageSendText = new JTextArea(5, MESSAGE_AREA_COLUMNS);
		messageSendScrollPane = new JScrollPane(messageSendText);
		
		messagePanel.add(messageTabbedPane);
		messagePanel.add(Box.createRigidArea(new Dimension(0,5)));
		messagePanel.add(messageSendLabel);
		messagePanel.add(messageSendScrollPane);
	}
	
	/**************************************
	 * Sets up the available clients panel.
	 **************************************/
	private void setupAvailableClientsPanel() {
		availableClientsPanel = new JPanel();
		availableClientsPanel.setLayout(new BoxLayout(availableClientsPanel, BoxLayout.Y_AXIS));
		
		availableClientsLabel = new JLabel("Connected Clients");
		
		availableClientsModel = new DefaultListModel<String>();
		availableClientsList = new JList<String>(availableClientsModel);
		availableClientsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		availableClientsScrollPane = new JScrollPane(availableClientsList);
		availableClientsScrollPane.setPreferredSize(new Dimension(5, 250));
		
		availableClientsPanel.add(availableClientsLabel);
		availableClientsPanel.add(availableClientsScrollPane);
		client.getClientList();
	}
	
	/****************************
	 * Sets up the button panel.
	 ****************************/
	private void setupButtonPanel() {
		buttonPanel = new JPanel();
		sendButton = new JButton("Send");
		sendButton.addActionListener(buttonListener);
		broadcastButton = new JButton("Broadcast");
		broadcastButton.addActionListener(buttonListener);
		
		buttonPanel.add(sendButton);
		buttonPanel.add(broadcastButton);
	}
	
	/****************************************************************
	 * Removes the selected tab from the display as long as it's not
	 * the broadcast tab.
	 *****************************************************************/
	private void removeSelectedTab() {
		Component selectedTab = messageTabbedPane.getSelectedComponent();
		if (selectedTab.equals(broadcastMessageScrollPane)) {
			return;
		} else {
			messageTabbedPane.remove(selectedTab);
		}
	}
	
	/*******************************************************************
	 * Updates the available clients model with the passed client list.
	 * @param clientList client list to update display with
	 *******************************************************************/
	private void updateClientList(final String[] clientList) {
		availableClientsModel.clear();
		for (String client : clientList) {
			availableClientsModel.addElement(client);
		}
	}
	
	/************************
	 * Shuts down the client.
	 ************************/
	private void exit() {
		client.exit();
		JOptionPane.showMessageDialog(frame,
			    "Terminating this client session.",
			    "Connection Failure",
			    JOptionPane.ERROR_MESSAGE);
		System.exit(0);
	}
	
	/********************************************************************
	 * Gets the IP of the selected client, adds an ADMIN_KICK tag to it,
	 * then sends the IP to the server to be kicked.
	 ********************************************************************/
	private void adminKick() {
		String kickClientIP = (String) availableClientsList.getSelectedValue(); //Client to kick
		client.adminKick(kickClientIP);
	}
	
	/*******************************************************************
	 * Gets the messaged typed by the user and adds a MESSAGE tag to it
	 * along with a tag signifying this client is the sender then it
	 * sends it to the server.
	 *******************************************************************/
	private void send() {
		String selectedClientIP = (String) availableClientsList.getSelectedValue(); //Client to send to
		String messageToSend = "#MESSAGE#<" + selectedClientIP + ">" + messageSendText.getText();
		if (client.sendMessage(messageToSend)) {
			boolean tabExist = false;
			int tabIndex = -1;
			for (int i = 0; i < messageTabbedPane.getTabCount(); i++) {
				if (messageTabbedPane.getTitleAt(i).equals(selectedClientIP)) {
					tabExist = true;
					tabIndex = i;
				}
			}
			if (tabExist) { //Add the message to exisiting tab
				JScrollPane tab = (JScrollPane) messageTabbedPane.getComponentAt(tabIndex);
				JViewport viewport = tab.getViewport(); 
				JTextArea textArea = (JTextArea)viewport.getView();
				String temp = textArea.getText();
				textArea.setText(temp + "Me: " + messageSendText.getText() + '\n');
			} else { //Create a new tab and add message to it
				JTextArea messageReceiveText = new JTextArea(MESSAGE_AREA_ROWS, MESSAGE_AREA_COLUMNS);
				messageReceiveText.setEditable(false);
				messageReceiveText.setText("Me: " + messageSendText.getText() + '\n');
				JScrollPane messageReceiveScrollPane = new JScrollPane(messageReceiveText);
				
				messageTabbedPane.add(selectedClientIP, messageReceiveScrollPane);
			}
			messageSendText.setText("");
		} else {
			messageFailed();
		}
	}
	
	/*********************************************************************
	 * Gets the messaged typed by the user and adds a BROADCAST tag to it
	 * then sends it to the server.
	 **********************************************************************/
	private void broadcast() {
		String messageToBroadcast = "#BROADCAST#" + messageSendText.getText();
		if (client.sendMessage(messageToBroadcast)) {
			messageSendText.setText("");
		} else {
			messageFailed();
		}
	}
	
	/***************************************************
	 * Creates a dialog box letting the user know their
	 * message failed to send.
	 ***************************************************/
	private void messageFailed() {
		JOptionPane.showMessageDialog(frame,
			    "Failed to send the message.",
			    "Message Error",
			    JOptionPane.ERROR_MESSAGE);
	}
	
	/*************************************************************
	 * Gets messages from the server and handles the information.
	 *************************************************************/
	private void getMessage() {
		String message = client.getMessage();
		//If message is null it means the client has been kicked from the server.
		if (message == null) {
			JOptionPane.showMessageDialog(frame,
				    "The other clients hate you.\n"
				    + "You have been voted off the island.\n"
				    + "Terminating client session.",
				    "Connection Failure",
				    JOptionPane.ERROR_MESSAGE);
					System.exit(0);
		}
		int index1 = message.indexOf("#");
		int index2 = message.indexOf("#", index1 + 1);
		//Get an identifier string to see what type of message it is
		String identifier = message.substring(index1 + 1, index2);
		/*
		 * IP
		 * Updates the current client label.
		 */
		if (identifier.equals("IP")) {
			//System.out.println("CLIENT GUI: IP HERE");
			clientLabel.setText("Current Client:  " + message.substring(index2 + 1) + "  ");
		}
		/*
		 * CLIENT_LIST
		 * Updates the client list display.
		 */
		else if (identifier.equals("CLIENT_LIST")) {
			//System.out.println("CLIENT GUI: CLIENT_LIST HERE");
			String[] clientList = message.substring(index2 + 1).split(",");
			updateClientList(clientList);
		}
		/*
		 * BROADCAST
		 * Displays the message in the broadcast tab.
		 */
		else if (identifier.equals("BROADCAST")) {
			//System.out.println("CLIENT GUI: BROADCAST HERE");
			int index3 = message.indexOf("<");
			int index4 = message.indexOf(">");
			String messageFrom = message.substring(index3 + 1, index4);
			String temp = broadcastMessageText.getText();
			broadcastMessageText.setText(temp + messageFrom + ": " + message.substring(index4 + 1) + '\n');
		}
		/*
		 * MESSAGE
		 * Displays the message in the tab that represents
		 * the client the message is from.
		 */
		else if (identifier.equals("MESSAGE")) {
			//System.out.println("CLIENT GUI: MESSAGE HERE");
			int index3 = message.indexOf("<");
			int index4 = message.indexOf(">");
			String messageFrom = message.substring(index3 + 1, index4);
			boolean tabExist = false;
			int tabIndex = -1;
			for (int i = 0; i < messageTabbedPane.getTabCount(); i++) {
				if (messageTabbedPane.getTitleAt(i).equals(messageFrom)) {
					tabExist = true;
					tabIndex = i;
				}
			}
			if (tabExist) { //Add the message to exisiting tab
				JScrollPane tab = (JScrollPane) messageTabbedPane.getComponentAt(tabIndex);
				JViewport viewport = tab.getViewport(); 
				JTextArea textArea = (JTextArea) viewport.getView();
				String temp = textArea.getText();
				textArea.setText(temp + messageFrom + ": " + message.substring(index4 + 1) + '\n');
			} else { //Create a new tab and add message to it
				JTextArea messageReceiveText = new JTextArea(MESSAGE_AREA_ROWS, MESSAGE_AREA_COLUMNS);
				messageReceiveText.setEditable(false);
				messageReceiveText.setText(messageFrom + ": " + message.substring(index4 + 1) + '\n');
				JScrollPane messageReceiveScrollPane = new JScrollPane(messageReceiveText);
				
				messageTabbedPane.add(messageFrom, messageReceiveScrollPane);
			}
		}
	}
	
	/***********************************************************
	 * Creates a dialog box with information on admin commands.
	 ***********************************************************/
	private void adminHelp() {
		JOptionPane.showMessageDialog(frame,
			    "ADMIN COMMANDS:\n"
			    + "-----------------------------------------------------\n"
			    + "Admin Kick: Kicks the selected client from the server.",
			    "Admin Commands",
			    JOptionPane.INFORMATION_MESSAGE);
	}
	
	/***********************************************************
	 * Creates a dialog box with information about the program.
	 ***********************************************************/
	private void about() {
		JOptionPane.showMessageDialog(frame,
			    "This is a client that connects to a server\n"
			    + "using a TCP socket. It is able to communicate\n"
			    + "with other clients connected to the server\n"
			    + "individually or broadcast a message to all the\n"
			    + "clients connected to the server.\n"
			    + "All communication is encrypted with AES\n"
			    + "in CBC mode.",
			    "About",
			    JOptionPane.INFORMATION_MESSAGE);
	}
	
	/********************************************
	 * Runs the ClientGUI.
	 * @param args arguments for the main method
	 ********************************************/
	public static void main(final String[] args) {
		ClientGUI gui = new ClientGUI();
	}
	
	/*********************************************************
	 * The ButtonListener class is an Action Listener for all
	 * the buttons in the GUI.
	 * @author Kyle Hekhuis
	 * @since 10/29/16
	 *********************************************************/
	private class ButtonListener implements ActionListener {
		public void actionPerformed(final ActionEvent e) {
			if (e.getSource() == updateClientsMenuItem) {
				client.getClientList();
			} else if (e.getSource() == removeTabMenuItem) {
				removeSelectedTab();
			} else if (e.getSource() == exitMenuItem) {
				exit();
			} else if (e.getSource() == adminKickMenuItem) {
				adminKick();
			} else if (e.getSource() == adminHelpMenuItem) {
				adminHelp();
			} else if (e.getSource() == aboutMenuItem) {
				about();
			} else if (e.getSource() == sendButton) {
				send();
			} else if (e.getSource() == broadcastButton) {
				broadcast();
			}
		}
	}
	
	/**************************************************************
	 * The MessageListener class is a Property Change Listener to
	 * get messages from the server.
	 * @author Kyle Hekhuis
	 * @since 11/6/16
	 **************************************************************/
	private class MessageListener implements PropertyChangeListener {
	    public void propertyChange(final PropertyChangeEvent e) {
	    	//System.out.println("CLIENT GUI: MessageListener HERE");
	    	String propertyName = e.getPropertyName();
	        if ("serverMessage".equals(propertyName)) {
	        	getMessage();
	        }
	    }
	}
}