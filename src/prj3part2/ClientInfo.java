package prj3part2;

/***********************************************************************
 * The ClientInfo class holds information on a client connected to
 * a sever. It keeps track of the clients IP and the thread they are on.
 * @author Kyle Hekhuis
 * @since 11/2/16
 **********************************************************************/
public final class ClientInfo {

	/** The IP for the client this object represents. */
	private String clientIP;
	/** The thread for the client this object represents. */
	private ClientListener clientThread;
	
	/************************************************************************
	 * Creates a new ClientInfo object with the passed client IP
	 * and thread.
	 * @param clientIP the IP for the client this object represents
	 * @param clientThread the thread for the client this object represents
	 ************************************************************************/
	public ClientInfo(final String clientIP, final ClientListener clientThread) {
		this.clientIP = clientIP.substring(1);
		this.clientThread = clientThread;
	}
	
	/*************************
	 * Returns the client IP.
	 * @return the client IP
	 ************************/
	public String getClientIP() {
		return clientIP;
	}
	
	/*****************************
	 * Returns the client thread.
	 * @return the client thread
	 *****************************/
	public ClientListener getClientThread() {
		return clientThread;
	}
}
