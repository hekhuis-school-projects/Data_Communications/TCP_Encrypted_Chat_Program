package prj3part1;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

/*******************************************************************
 * The Server class is a server for a TCP encrypted chat server.
 * It receives encrypted messages from a client and then forwards
 * the message to the designated client or broadcast to all clients
 * connected to the server.
 * @author Kyle Hekhuis
 * @since 10/29/16
 ********************************************************************/
public final class Server {

	/** A thread safe hash map containing all clients connected to this server. */
	public static volatile ConcurrentHashMap<Long, ClientInfo> connectedClients;
	
	/*******************************
	 * Creates a new Server object.
	 *******************************/
	@SuppressWarnings("resource")
	private Server() {
		//Reader for user input
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(System.in));
		Boolean validPort = false;
		String portNumber = "";
		ServerSocket listenSocket = null;
		System.out.println("SERVER: Please enter a port number to listen on: ");
		//Getting a port number for the server from the user and checks if port is valid
		while (!validPort) {
			try {
				portNumber = inFromUser.readLine();
				listenSocket = new ServerSocket(Integer.parseInt(portNumber));
				System.out.println("SERVER: Listening for connections on port " + portNumber + ".");
				validPort = true;
			} catch (NumberFormatException | IOException e1) {
				System.out.println("SERVER: Not a valid port to use / Port unavailable.\n"
						+ "Please enter a different port number: ");
			}
		}
		//Gets a client connection and starts a new thread for each client
	    Socket clientSocket = null;
	    ClientListener cl;
	    connectedClients = new ConcurrentHashMap<Long, ClientInfo>();
	    while (true) {  
	    	try {
				clientSocket = listenSocket.accept();
				System.out.println("SERVER: Client connected.");
				cl = new ClientListener(clientSocket, clientSocket.getRemoteSocketAddress().toString());
				cl.start();
				//Adds connected client to the connected clients list
				connectedClients.put(cl.getId(), new ClientInfo(clientSocket.getRemoteSocketAddress().toString(), cl));
				/*
				int count = 1;
				for(Entry<Long, ClientInfo> entry : connectedClients.entrySet()) {
					System.out.println("SERVER: Client: " + count);
					count++;
					System.out.println("SERVER: Client Key: " + entry.getKey());
				    System.out.println("SERVER: Client IP: " + entry.getValue().clientIP);
				
				}
				*/
			} catch (IOException e) {
				System.out.println("SERVER: Failed to accept client on socket.");
			}
    	}
	}
	
	/*********************************************
	 * Runs the server.
	 * @param args arguments for the main method
	 ********************************************/
	public static void main(final String[] args) {
		Server s = new Server();
	}
}
 /***********************************************************************
  * The ClientListener class is a thread that listens for messages from
  * a connected client and reads identifier tags on the messages then
  * decides what to do with them based on the tags.
  * @author Kyle Hekhuis
  * @since 10/29/16
  ************************************************************************/
final class ClientListener extends Thread {
	
	/** Socket to listen on. */
	private Socket clientSocket;
	/** The IP of the socket this thread listens to. */
	private String thisClientIP;
	/** Input stream of info from client to server.  */
	private BufferedReader inFromClient;
	/** Output stream of info from server to client. */
	private DataOutputStream outToClient;
	
	/******************************************************************
	 * Creates a ClientListener object from the passed socket and IP.
	 * @param connection the socket this thread listens to
	 * @param clientIP the IP of the socket this thread listens to
	 ******************************************************************/
	ClientListener(final Socket connection, final String clientIP) {
		this.clientSocket = connection;
		this.thisClientIP = clientIP.substring(1);
	}
	
	/*******************************************************************
	 * Gets messages from the clientSocket and handles the information.
	 *******************************************************************/
	public void run() {
		String clientMessage = "";
		try {
			inFromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			outToClient = new DataOutputStream(clientSocket.getOutputStream());
			while (true) {
				clientMessage = inFromClient.readLine();
				int index1 = clientMessage.indexOf("#");
				int index2 = clientMessage.indexOf("#", index1 + 1);
				//Get an identifier string to see what type of message it is
				String identifier = clientMessage.substring(index1 + 1, index2);
				//System.out.println("SERVER: Identifier: " + identifier);
				/*
				 * IP
				 * Get the IP of this client and send back to them.
				 */
				if (identifier.equals("GET_IP")) {
					//System.out.println("SERVER: IP HERE");
					forwardMessage("#IP#" + thisClientIP);
				}
				/*
				 * GET_CLIENTS
				 * Get list of clients connected to server and send list to client.
				 */
				else if (identifier.equals("GET_CLIENTS")) {
					//System.out.println("SERVER: GET_CLIENTS HERE");
					String clientList = "#CLIENT_LIST#";
					for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
					    clientList += entry.getValue().getClientIP() + ",";
					    forwardMessage(clientList);
					}
				}
				/*
				 * TERMINATE
				 * End this connection.
				 */
				else if (identifier.equals("TERMINATE")) {
					//System.out.println("SERVER: TERMINATE HERE");
					Server.connectedClients.remove(currentThread().getId());
					clientSocket.close();
				}
				/*
				 * ADMIN_KICK
				 * Disconnect the desired client from the server and remove
				 * them from server's connected clients list.
				 */
				else if (identifier.equals("ADMIN_KICK")) {
					//System.out.println("SERVER: ADMIN_KICK HERE");
					String clientToKick = clientMessage.substring(index2 + 1);
					Long clientToKickKey = null;
					for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
					    if (entry.getValue().getClientIP().equals(clientToKick)){
					    	clientToKickKey = entry.getKey();
					    }
					}
					try {
						//Server.connectedClients.get(clientToKickKey).clientThread.interrupt();
						Server.connectedClients.get(clientToKickKey).getClientThread().getClientSocket().close();
						Server.connectedClients.remove(clientToKickKey);
					} catch (Exception e) {
						System.out.println("SERVER: Failed to kick client.");
					}
					
				}
				/*
				 * BROADCAST
				 * Send this message out to every client connected to the server
				 * and add a tag of which client it's from.
				 */
				else if (identifier.equals("BROADCAST")) {
					//System.out.println("SERVER: BROADCAST HERE");
					String newMessage = "#BROADCAST#<" + thisClientIP + ">" + clientMessage.substring(index2 + 1);
					for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
						entry.getValue().getClientThread().forwardMessage(newMessage);
					}
				}
				/*
				 * MESSAGE
				 * Send this message to the designated client and add a tag
				 * of which client it's from.
				 */
				else if (identifier.equals("MESSAGE")) {
					//System.out.println("SERVER: MESSAGE HERE");
					int index3 = clientMessage.indexOf("<");
					int index4 = clientMessage.indexOf(">");
					String clientToSendTo = clientMessage.substring(index3 + 1, index4);
					String newMessage = "#MESSAGE#<" + thisClientIP + ">" + clientMessage.substring(index4 +1);
					for (Entry<Long, ClientInfo> entry : Server.connectedClients.entrySet()) {
					    if (entry.getValue().getClientIP().equals(clientToSendTo)) {
					    	entry.getValue().getClientThread().forwardMessage(newMessage);
					    }
					}
				}
	    	}
		} catch (Exception e) {
			//Remove this client if any errors occur
			Server.connectedClients.remove(currentThread().getId());
			System.out.println("SERVER: Client disconnected.");
		}
	}
	
	/**********************************************
	 * Forwards a message out to the client.
	 * @param message message to forward to client
	 **********************************************/
	public void forwardMessage(final String message) {
		try {
			//System.out.println("SERVER: Message to send: " + '\n' + message);
			outToClient.writeBytes(message + '\n');
			//System.out.println("SERVER: Message Forwarded");
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	/***************************************
	 * Returns clientSocket for this thread.
	 * @return clientSocket of this thread
	 ***************************************/
	public Socket getClientSocket() {
		return clientSocket;
	}
}
