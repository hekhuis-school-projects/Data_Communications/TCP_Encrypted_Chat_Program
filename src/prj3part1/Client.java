package prj3part1;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.util.Timer;
import java.util.TimerTask;

/****************************************************************************
 * The Client class is a client that connects to a server and is able
 * to get a list of other clients connected to the server, send a message to
 * an individual client or all clients at once, receive messages from the 
 * server, and kick selected clients from the server.
 * @author Kyle Hekhuis
 * @since 10/29/16
 *****************************************************************************/
public final class Client {
	
	/** Used to listen for updates to serverMessage. */
	private PropertyChangeSupport changes = new PropertyChangeSupport(this); 
	/** Socket that client uses to connect to server. */
	private Socket clientSocket;
	/** Input stream of info from server to client. */
	private BufferedReader inFromServer;
	/** Output stream of info from client to server. */
	private DataOutputStream outToServer;
	/** Message from the server. */
	private volatile String serverMessage;
	
	/***************************************************************************
	 * Creates a Client object that connects to the server located at
	 * the passed IP and port number.
	 * @param ip IP of server to connect to
	 * @param port port number of server to connect to
	 * @throws Exception failed to create socket or create input/output stream
	 **************************************************************************/
	public Client(final String ip, final String port) throws Exception {
		//InetAddress IPAddress = InetAddress.getByName(ip);
		clientSocket = new Socket(InetAddress.getByName(ip), Integer.parseInt(port));
		outToServer = new DataOutputStream(clientSocket.getOutputStream());
		inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
		ServerListener sl = new ServerListener();
		sl.start();
	}
	
	/*********************************************
	 * Ask the server for the IP of this client.
	 *********************************************/
	public void getClientIP() {
		try {
			//System.out.println("CLIENT: GET_IP HERE");
			outToServer.writeBytes("#GET_IP#\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/****************************************************
	 * Ask the server for its list of connected clients.
	 ****************************************************/
	public void getClientList() {
		//Uncomment gcl and comment/delete everything else in
		//this method if update client list via timer is desired.
		//GetClientsTimer gcl = new GetClientsTimer();
		try {
			//System.out.println("CLIENT: GET_CLIENTS HERE");
			outToServer.writeBytes("#GET_CLIENTS#\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*************************************************
	 * Tells the server this client is shutting down.
	 *************************************************/
	public void exit() {
		try {
			//System.out.println("CLIENT: TERMINATE HERE");
			outToServer.writeBytes("#TERMINATE#\n");
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/***********************************************************
	 * Ask the server to kick the client connected to it whose
	 * IP is the one passed.
	 * @param clientIP IP of client to kick from server
	 ***********************************************************/
	public void adminKick(final String clientIP) {
		try {
			//System.out.println("CLIENT: ADMIN_KICK HERE: " + clientIP);
			outToServer.writeBytes("#ADMIN_KICK#" + clientIP + '\n');
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/*********************************************************
	 * Sends the passed message to the server.
	 * @param message message to send to server
	 * @return true if message is successfully sent to server
	 *********************************************************/
	public boolean sendMessage(final String message) {
		try {
			//System.out.println("CLIENT: MESSAGE HERE: " + message);
			outToServer.writeBytes(message + '\n');
			return true;
		} catch (IOException e) {
			return false;
		}
	}
	
	/***************************************************************
	 * Receives message from server, sets serverMessage to the
	 * received message, and then notifies anyone listening to this
	 * class that serverMessage has been updated.
	 ***************************************************************/
	private void receiveMessage() {
		serverMessage = "";
		try {
			//System.out.println("CLIENT: RECEIVING MESSAGE HERE");
			String tempOldMessage = serverMessage.toString();
			serverMessage = inFromServer.readLine();
			//System.out.println("CLIENT: MESSAGE RECEIVED HERE: " + serverMessage);
			//Tells any listeners that serverMessage has been updated
			changes.firePropertyChange(new PropertyChangeEvent(this, "serverMessage", tempOldMessage, serverMessage));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/***************************************
	 * Returns the message from the server.
	 * @return the message from the server
	 ***************************************/
	public String getMessage() {
		return serverMessage;
	}

	/************************************************************
	 * Adds a property change listener to this object.
	 * @param pcl property change listener to add to this object
	 ************************************************************/
	public void addPropertyChangeListener(final PropertyChangeListener pcl) {
        changes.addPropertyChangeListener(pcl);
    }

	/*****************************************************************
	 * Removes a property change listener from this object.
	 * @param pcl property change listener to remove from this object
	 ******************************************************************/
    public void removePropertyChangeListener(final PropertyChangeListener pcl) {
        changes.removePropertyChangeListener(pcl);
    }
    
    /******************************************************************
     * The ServerListener class is a thread that listens for messages
     * from the server this client is connected to.
     * @author Kyle Hekhuis
     * @since 10/1/16
     ******************************************************************/
    private class ServerListener extends Thread {
		/**********************************
		 * Creates a ServerListener object.
		 **********************************/
		ServerListener() {}
		
		/***********************************************
		 * Constantly listens for messages from server.
		 ***********************************************/
		public void run() {
			try{
				while (true) {
					receiveMessage();
				}
			} catch (Exception e) {
				this.interrupt();
			}
		}
	}
    
//    /****************************************************************
//     * The GetClientsTimer class is a timer that will get an updated
//     * clients list from the server at a fixed rate.
//     * @author Kyle Hekhuis
//     * @since 11/6/16
//     ****************************************************************/
//    private final class GetClientsTimer {
//    	/************************************
//    	 * Creates a GetClientsTimer object.
//    	 ************************************/
//    	private GetClientsTimer() {
//    		Timer timer = new Timer();
//    		//Period of time in milliseconds between getting client list
//    		int timeDelay = 1000;
//    		timer.scheduleAtFixedRate(new GetClientsTask(), 0, timeDelay);
//    	}
//    	
//    	/**************************************************************
//    	 * The GetClientsTask class is a timer task to ask the server
//    	 * for its client list.
//    	 **************************************************************/
//    	private class GetClientsTask extends TimerTask {
//    		public void run() {
//    			try {
//    				//System.out.println("CLIENT: GET_CLIENTS HERE");
//    				outToServer.writeBytes("#GET_CLIENTS#\n");
//    			} catch (IOException e) {
//    				e.printStackTrace();
//    			}
//    		}
//    	}
//    }
}