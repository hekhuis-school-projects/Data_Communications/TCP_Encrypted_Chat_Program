TCP Encrypted Chat Program
Language: Java
Author: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 457 - Data Communications
Semester / Year: Fall 2016

This program creates a server / multi-client setup chat system. The clients
are able to see all other clients connected to the server and send messages
to them. All messages are encrypted using AES.

For full specifications see 'CIS 457 Project 4_ TCP Encrypted Chat Program.pdf'.

Usage:
Run 'Server.java' and follow the input prompts to set up the server.
After the server is running, run 'ClientGUI.java' for each client you wish
to connect and follow the input prompts to connect to the server.
After clients are connected, you can use the GUI to send messages.